'use client';

export const getTotalAverage = (onlineHistory) => {
  const history = onlineHistory.history;
  if (history) {
    return Math.ceil(
      history.reduce((sum, item) => sum + item.count, 0) / history.length
    );
  }
  return 0;
};

export const getAverageFrom = (onlineHistory, days) => {
  const history = onlineHistory.history;
  if (history) {
    let historyChunk = history.slice(days * -1);
    return Math.ceil(
      historyChunk.reduce((sum, item) => sum + item.count, 0) /
        historyChunk.length
    );
  }
  return 0;
};

export const getOnlinePlayers = (data) => {
  if (data) {
    return data.reduce((total, item) => total + item.players, 0);
  }
  return 0;
};
