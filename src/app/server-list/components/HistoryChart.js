'use client';

import dynamic from 'next/dynamic';

const Chart = dynamic(() => import('@/app/components/Chart'), { ssr: false });

const HistoryChart = ({ onlineHistory }) => {
  return (
    <>
      <noscript>
        We cannot load data chart, please enable java script...
      </noscript>
      <Chart data={onlineHistory.history} />
    </>
  );
};

export default HistoryChart;
