'use client';

import { useState, useContext, createContext } from 'react';

const DataContext = createContext([], () => {});
export const useData = () => useContext(DataContext);

const Data = ({ children }) => {
  const [desc, setDesc] = useState(null);
  return (
    <DataContext.Provider value={[desc, setDesc]}>
      {children}
    </DataContext.Provider>
  );
};

export default Data;
