'use client';

import styles from '@/app/styles/server-list/stats.module.css';

import { useEffect, useState } from 'react';
import {
  getTotalAverage,
  getAverageFrom,
  getOnlinePlayers,
} from '@/app/server-list/components/Utils';
import { useData } from '@/app/server-list/[address]/Data';

const Stats = ({ address, onlineHistory }) => {
  const [serverData, setServerData] = useState(null);
  const [, setDesc] = useData();
  useEffect(() => {
    fetch(
      `https://api.gothic-online.com.pl/v2/master/server/${address}/details/`
    )
      .then((res) => {
        if (!res.ok) {
          throw new Error('Failed to fetch data');
        }
        return res.json();
      })
      .then((data) => {
        setServerData(data);
        setDesc(data.description);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <>
      <h2 className={styles.h2}>Servers stats for IP: {address}</h2>
      {serverData ? (
        <>
          <p className={styles.p}>
            <span className={styles.mark}>Hostname:</span> {serverData.hostname}
          </p>
          <p className={styles.p}>
            <span className={styles.mark}>Version:</span> {serverData.version}
          </p>
        </>
      ) : (
        <p className={styles.p}>We could not load data, try again later!</p>
      )}
      <hr className={styles.hr} />
      <p className={styles.p}>
        <span className={styles.mark}>Average players (1 month):</span>{' '}
        {getTotalAverage(onlineHistory)}
      </p>
      <p className={styles.p}>
        <span className={styles.mark}>Average players (7 days):</span>{' '}
        {getAverageFrom(onlineHistory, 7)}
      </p>
      <p className={styles.p}>
        <span className={styles.mark}>Average players (3 days):</span>{' '}
        {getAverageFrom(onlineHistory, 3)}
      </p>
      <p className={styles.p}>
        <span className={styles.mark}>Peak players:</span>{' '}
        {onlineHistory.peak.count} from {onlineHistory.peak.day}
      </p>
      <hr className={styles.hr} />
      {serverData ? (
        <p className={styles.p}>
          <span className={styles.mark}>Online players:</span>{' '}
          {`${serverData.players}/${serverData.max_slots}`}
        </p>
      ) : (
        <p className={styles.p}>We could not load data, try again later!</p>
      )}
    </>
  );
};

export default Stats;
