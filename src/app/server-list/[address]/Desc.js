'use client';

import { useData } from '@/app/server-list/[address]/Data';

const Desc = () => {
  const [desc] = useData();
  return <div dangerouslySetInnerHTML={{ __html: desc }} />;
};

export default Desc;
