import styles from '@/app/styles/server-list/address.module.css';

import { notFound } from 'next/navigation';

import Data from '@/app/server-list/[address]/Data';
import Stats from '@/app/server-list/[address]/Stats';
import Desc from '@/app/server-list/[address]/Desc';
import HistoryChart from '@/app/server-list/components/HistoryChart';

const validateIPAddress = (ipAddress) => {
  const ipPattern = /^([0-9]{1,3}\.){3}[0-9]{1,3}(:[0-9]{1,5})?$/;
  if (!ipPattern.test(ipAddress)) {
    return false;
  }
  const parts = ipAddress.split(':');
  const octetList = parts[0].split('.');
  for (let i = 0; i < octetList.length; i++) {
    const octet = parseInt(octetList[i], 10);
    if (isNaN(octet) || octet < 0 || octet > 255) {
      return false;
    }
  }
  const portPart = parts[1];
  if (portPart) {
    const port = parseInt(portPart, 10);
    if (isNaN(port) || port < 0 || port > 65535) {
      return false;
    }
  }
  return true;
};

const getOnlineHistory = async (address) => {
  const res = await fetch(
    `https://api.gothic-online.com.pl/v2/master/server/${address}/online-history/`,
    { next: { revalidate: 120 } }
  );
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
  return res.json();
};

const ServerInfo = async ({ params }) => {
  const address = decodeURIComponent(params.address);
  if (!validateIPAddress(address)) {
    notFound();
  }
  const onlineHistoryData = await getOnlineHistory(address);
  if (!onlineHistoryData || onlineHistoryData.peak === null) {
    notFound();
  }
  return (
    <section className={styles.section}>
      <div className={styles.container}>
        <header className={styles.header}>
          <h1 className={styles.h1}>Server informations</h1>
        </header>
        <div className={styles.component}>
          <Data>
            <div className={styles.data}>
              <div className={styles.block}>
                <HistoryChart onlineHistory={onlineHistoryData} />
              </div>
              <div className={styles.block}>
                <Stats address={address} onlineHistory={onlineHistoryData} />
              </div>
            </div>
            <div className={styles.desc}>
              <Desc />
            </div>
          </Data>
        </div>
      </div>
    </section>
  );
};

export default ServerInfo;
