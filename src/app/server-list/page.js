import styles from '@/app/styles/server-list/server-list.module.css';

import Table from '@/app/server-list/Table';
import Stats from '@/app/server-list/Stats';
import HistoryChart from '@/app/server-list/components/HistoryChart';

const getOnlineHistory = async () => {
  const res = await fetch(
    'https://api.gothic-online.com.pl/v2/master/servers/online-history/',
    { next: { revalidate: 120 } }
  );
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }
  return res.json();
};

const ServerList = async () => {
  const onlineHistoryData = await getOnlineHistory();
  return (
    <section className={styles.section}>
      <div className={styles.container}>
        <header className={styles.header}>
          <h1 className={styles.h1}>List of servers</h1>
          <div className={styles.subtext}>
            <h2 className={styles.h2}>One of the servers is yours?</h2>
            <p className={styles.p}>
              Stand out on the list and set the appropriate description for it
              by configuring the server!
            </p>
          </div>
        </header>
        <div className={styles.component}>
          <Table />
        </div>
        <div className={styles.component}>
          <div className={styles.data}>
            <div className={styles.block}>
              <HistoryChart onlineHistory={onlineHistoryData} />
            </div>
            <div className={styles.block}>
              <Stats onlineHistory={onlineHistoryData} />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ServerList;
