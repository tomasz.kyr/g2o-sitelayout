'use client';

import styles from '@/app/styles/server-list/stats.module.css';

import { useEffect, useState } from 'react';
import {
  getTotalAverage,
  getAverageFrom,
  getOnlinePlayers,
} from '@/app/server-list/components/Utils';

const Stats = ({ onlineHistory }) => {
  const [listData, setListData] = useState(null);
  useEffect(() => {
    fetch('https://api.gothic-online.com.pl/v2/master/servers/')
      .then((res) => {
        if (!res.ok) {
          throw new Error('Failed to fetch data');
        }
        return res.json();
      })
      .then((data) => {
        setListData(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <>
      <h2 className={styles.h2}>Servers stats</h2>
      <p className={styles.p}>
        Average players (1 month): {getTotalAverage(onlineHistory)}
      </p>
      <p className={styles.p}>
        Average players (7 days): {getAverageFrom(onlineHistory, 7)}
      </p>
      <p className={styles.p}>
        Average players (3 days): {getAverageFrom(onlineHistory, 3)}
      </p>
      <p className={styles.p}>
        Peak players: {onlineHistory.peak.count} (from {onlineHistory.peak.day})
      </p>
      <hr className={styles.hr} />
      {listData ? (
        <>
          <p className={styles.p}>
            Online players: {getOnlinePlayers(listData)}
          </p>
          <p className={styles.p}>Active servers: {listData.length}</p>
        </>
      ) : (
        <p className={styles.p}>We could not load data, try again later!</p>
      )}
    </>
  );
};

export default Stats;
