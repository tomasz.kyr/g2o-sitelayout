'use client';

import styles from '@/app/styles/server-list/table.module.css';

import { useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';

import { motion, AnimatePresence } from 'framer-motion';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import debounce from 'lodash.debounce';

import { faUsers } from '@fortawesome/free-solid-svg-icons';

import avatar from '@/assets/avatar.webp';

const Table = () => {
  const [serverList, setServerList] = useState(null);
  const [filteredList, setFilteredList] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const filterBySearch = debounce((event, serverList, setFilteredList) => {
    if (serverList === null) {
      return;
    }
    setFilteredList(
      [...serverList].filter((item) =>
        item.hostname.toLowerCase().includes(event.target.value.toLowerCase())
      )
    );
  }, 300);
  const handleChange = (event) => {
    filterBySearch(event, serverList, setFilteredList);
  };
  useEffect(() => {
    fetch('https://api.gothic-online.com.pl/v2/master/servers/')
      .then((res) => {
        if (!res.ok) {
          throw new Error('Failed to fetch data');
        }
        return res.json();
      })
      .then((data) => {
        setServerList(data);
        setFilteredList(data);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => setLoading(false));
  }, []);
  return (
    <>
      <div className={styles.search}>
        <input
          className={styles.search__input}
          onChange={handleChange}
          type="text"
          placeholder="Search server by name..."
        />
      </div>
      <div
        className={
          isLoading
            ? [styles.container, styles.loading].join(' ')
            : styles.container
        }
      >
        <table className={styles.list}>
          <tbody>
            {filteredList.length !== 0 ? (
              <AnimatePresence initial={false}>
                {filteredList
                  .sort((a, b) => b.players - a.players)
                  .map((server, index) => (
                    <motion.tr
                      className={styles.row}
                      key={index}
                      initial={{ opacity: 0 }}
                      animate={{ opacity: 1 }}
                      exit={{ opacity: 0 }}
                      transition={{
                        opacity: { duration: 0.25 },
                      }}
                    >
                      <td
                        className={[styles.cell, styles['cell--icon']].join(
                          ' '
                        )}
                      >
                        <Image
                          width="50"
                          height="50"
                          alt="server icon"
                          className={styles.icon}
                          src={avatar}
                        />
                      </td>
                      <td
                        className={[styles.cell, styles['cell--host']].join(
                          ' '
                        )}
                      >
                        <Link
                          className={styles.link}
                          href={'/server-list/' + server.address}
                        >
                          {server.hostname}
                        </Link>
                      </td>
                      <td
                        className={[styles.cell, styles['cell--slots']].join(
                          ' '
                        )}
                      >
                        <FontAwesomeIcon
                          className={styles.slots__icon}
                          icon={faUsers}
                        />
                        <span className={styles.slots__text}>
                          {server.players}/{server.max_slots}
                        </span>
                      </td>
                      <td
                        className={[styles.cell, styles['cell--version']].join(
                          ' '
                        )}
                      >
                        {server.version}
                      </td>
                      <td
                        className={[styles.cell, styles['cell--join']].join(
                          ' '
                        )}
                      >
                        <button
                          onClick={() =>
                            (window.location =
                              'g2o://' +
                              server.address +
                              '?nickname=webuser' +
                              new Date().getMilliseconds())
                          }
                        >
                          Play
                        </button>
                      </td>
                    </motion.tr>
                  ))}
              </AnimatePresence>
            ) : (
              <AnimatePresence initial={false}>
                <motion.tr
                  className={styles.row}
                  key={'null'}
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                  transition={{
                    opacity: { duration: 0.25 },
                  }}
                >
                  <td className={[styles.cell, styles['cell--icon']].join(' ')}>
                    <Image
                      width="50"
                      height="50"
                      alt="server icon"
                      className={styles.icon}
                      src={avatar}
                    />
                  </td>
                  <td className={styles['cell--empty']} colSpan={4}>
                    {!isLoading
                      ? 'We cannot find requested data...'
                      : 'Trying to fetch data...'}
                  </td>
                </motion.tr>
              </AnimatePresence>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Table;
