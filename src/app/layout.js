import '@/app/styles/global/globals.css';
import '@/app/styles/global/recharts.css';

import { Montserrat } from 'next/font/google';

const montserrat = Montserrat({ subsets: ['latin'] });

import '@fortawesome/fontawesome-svg-core/styles.css';
import { config } from '@fortawesome/fontawesome-svg-core';

config.autoAddCss = false;

export const metadata = {
  viewport: {
    width: 'device-width',
    initialScale: 1,
    maximumScale: 2,
  },
  themeColor: [
    { media: '(prefers-color-scheme: light)', color: '#131313' },
    { media: '(prefers-color-scheme: dark)', color: '#131313' },
  ],
  icons: {
    icon: '/favicon.ico',
  },
  title: 'Gothic Online',
  description:
    'Gothic 2 Online (G2O) is a free multiplayer modification for popular RPG Gothic II:Night of the Raven 2.6 Originally founded in early 2016 as a replacement for almost dead GMP/GMP:A. Since then growing into advanced multiplayer platform. Multiplayer was build using reverse engineering techniques with own SDK, later changed to AST SDK. Gothic 2 Online exposes original games engine functionality via a scripting language Squirrel. Using scripts a minimal sandbox style game, can be changed to any game mode. Scripting API can be extended by external modules. Both server and client supports scripting and modules API. The project is developed and maintained by G2O Team and remains closed-source for security reasons.',
  keywords:
    'gothic-online, gothic-multiplayer, gothic 2, gothic, mmo, mmorpg, gothic mmo, gothic serwer, gothic forum, gothic wiki, gothic docs gothic online forum',
};

import ModalProvider from '@/app/components/modal/ModalProvider';

import Navbar from '@/app/Navbar';
import Footer from '@/app/Footer';

import { PageWrapper } from '@/app/components/PageTransition';

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={montserrat.className}>
        <ModalProvider>
          <Navbar />
          <main>
            <PageWrapper>{children}</PageWrapper>
          </main>
          <Footer />
        </ModalProvider>
      </body>
    </html>
  );
}
