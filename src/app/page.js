import Hero from '@/app/Hero';
import InfoBox from '@/app/Infobox';
import QuestionBox from '@/app/Questionbox';

const Home = () => (
  <>
    <Hero />
    <InfoBox />
    <QuestionBox />
  </>
);

export default Home;
