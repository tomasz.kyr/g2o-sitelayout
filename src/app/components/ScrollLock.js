'use client';

import { useEffect } from 'react';

const ScrollLock = () => {
  const disableScroll = () => {
    document.body.style.overflow = 'hidden';
  };
  const enableScroll = () => {
    document.body.style.overflow = 'auto';
  };
  useEffect(() => {
    disableScroll();
    return () => {
      enableScroll();
    };
  }, []);
  return;
};

export default ScrollLock;
