'use client';

import { AnimatePresence, motion } from 'framer-motion';
import { usePathname } from 'next/navigation';

import { LayoutRouterContext } from 'next/dist/shared/lib/app-router-context';
import { useContext, useRef } from 'react';

export const PageMotion = ({ children }) => {
  const context = useContext(LayoutRouterContext);
  const frozen = useRef(context).current;
  return (
    <LayoutRouterContext.Provider value={frozen}>
      {children}
    </LayoutRouterContext.Provider>
  );
};

export const PageWrapper = ({ children }) => {
  const pathname = usePathname();
  return (
    <AnimatePresence initial={false} mode="wait">
      <motion.div
        key={pathname}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.4, type: 'tween' }}
      >
        <PageMotion>{children}</PageMotion>
      </motion.div>
    </AnimatePresence>
  );
};
