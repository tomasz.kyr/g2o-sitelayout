'use client';

import { AnimatePresence, motion } from 'framer-motion';

import { useState, useContext, createContext } from 'react';
import dynamic from 'next/dynamic';

import Modal from '@/app/components/modal/Modal';
import Download from '@/app/components/modal/Download';

const Rules = dynamic(
  () => {
    import('react-pdf/dist/esm/Page/TextLayer.css');
    import('react-pdf/dist/esm/Page/AnnotationLayer.css');
    // returns component but before it, we load pdf CSS...
    return import('@/app/components/modal/Rules');
  },
  {
    loading: () => <p>Loading pdf module...</p>,
  }
);

const ModalProviderContext = createContext([], () => {});
export const useModalContext = () => useContext(ModalProviderContext);

const ModalProvider = ({ children }) => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [downloadLink, setDownloadLink] = useState(null);
  return (
    <ModalProviderContext.Provider
      value={{ isModalOpen, setModalOpen, downloadLink, setDownloadLink }}
    >
      <AnimatePresence>
        {isModalOpen && (
          <motion.div
            style={{
              position: 'relative',
              zIndex: 99,
            }}
            key="modal"
            initial={{ opacity: 0 }}
            exit={{ opacity: 0 }}
            animate={{ opacity: 1 }}
          >
            <Modal>{downloadLink ? <Rules /> : <Download />}</Modal>
          </motion.div>
        )}
      </AnimatePresence>
      {children}
    </ModalProviderContext.Provider>
  );
};

export default ModalProvider;
