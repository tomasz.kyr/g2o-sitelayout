'use client';

import styles from '@/app/styles/modal.module.css';

import { useModalContext } from '@/app/components/modal/ModalProvider';
import useSWR from 'swr';

const preFetchData = `{"name":"0.2.1","items":[{"key":"installer","title":"Gothic 2 Online","description":"Gothic 2 Online game installer","download_url":""},{"key":"server-windows","title":"Windows Server x86","description":"32-bit version of the server for the Windows platform","download_url":""},{"key":"server-windows","title":"Windows Server x64","description":"64-bit version of the server for the Windows platform","download_url":""},{"key":"server-linux","title":"Linux Server x86","description":"32-bit version of the server for the GNU Linux platform","download_url":""},{"key":"server-linux","title":"Linux Server x64","description":"64-bit version of the server for the GNU Linux platform","download_url":""},{"key":"server-linux","title":"Linux Server ARM x86","description":"32-bit version of the server for the ARM Linux platform","download_url":""},{"key":"server-linux","title":"Linux Server ARM x64","description":"64-bit version of the server for the ARM Linux platform","download_url":""}]}`;

const groupByKey = (list, key) =>
  list.reduce(
    (hash, object) => ({
      ...hash,
      [object[key]]: (hash[object[key]] || Array()).concat(object),
    }),
    Object()
  );

const useData = () => {
  const { data, error } = useSWR(
    'https://api.gothic-online.com.pl/v2/version/releases/latest/',
    (...args) => fetch(...args).then((res) => res.json()),
    { fallbackData: JSON.parse(preFetchData) }
  );

  return {
    data: data && {
      version: data?.name,
      groups: groupByKey(data.items, 'key'),
    },
    isError: error,
  };
};

const Download = () => {
  const { data, isError } = useData();
  const { setDownloadLink } = useModalContext();
  const translate = (key) => {
    let translation;
    switch (key) {
      case 'installer':
        translation = 'Game client';
        break;
      case 'server-windows':
        translation = 'Windows Server';
        break;
      case 'server-linux':
        translation = 'Linux Server';
        break;
      default:
        translation = key;
    }
    return translation;
  };
  return isError ? (
    <div className={styles.head}>
      <h2 className={styles.error}>
        We could not load data, please try again later!
      </h2>
    </div>
  ) : (
    data &&
      Object.keys(data.groups).map((group) => (
        <div key={group} className={styles.group}>
          <h3 className={styles.group__title}>{translate(group)}</h3>
          {data.groups[group].map((item, index) => (
            <div key={index} className={styles.item__row}>
              <div className={styles.item__container}>
                <h3 className={styles.item__title}>
                  {item.title}{' '}
                  <span className={styles.item__version}>{data.version}</span>
                </h3>
                <p className={styles.item__description}>
                  {item.description} {item.name}
                </p>
              </div>
              <button onClick={() => setDownloadLink(item.download_url)}>
                Download
              </button>
            </div>
          ))}
        </div>
      ))
  );
};

export default Download;
