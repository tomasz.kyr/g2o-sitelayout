'use client';

import styles from '@/app/styles/modal.module.css';

import { useState, useEffect, useRef } from 'react';
import { pdfjs, Document, Page } from 'react-pdf';

pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/build/pdf.worker.min.js`;

import { useModalContext } from '@/app/components/modal/ModalProvider';

const Rules = () => {
  const { setModalOpen, downloadLink, setDownloadLink } = useModalContext();
  const [pages, setPages] = useState([]);
  const [width, setWidth] = useState(0);
  const ref = useRef(null);
  const onDocumentLoadSuccess = ({ numPages }) => {
    let arr = [];
    for (let page = 1; page <= numPages; page++) {
      arr.push(page);
    }
    setPages(arr);
  };
  useEffect(() => {
    const updateWitdth = () => {
      setWidth(ref.current.offsetWidth);
    };
    updateWitdth();
    window.addEventListener('resize', updateWitdth);
    return () => {
      if (downloadLink !== null) {
        setDownloadLink(null);
      }
      window.removeEventListener('resize', updateWitdth);
    };
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <>
      <hr className={styles.hrtop} />
      <div className={styles.head}>
        <h1 className={styles.title}>
          Before download, read our Terms of Service...
        </h1>
        <p className={styles.subtitle}>
          To download file you must scroll down, and agree to our terms of
          service!
        </p>
      </div>
      <div ref={ref} className={styles.document}>
        <Document
          className={styles.pdfview}
          file={`/rules.pdf`}
          onLoadError={console.error}
          onLoadSuccess={onDocumentLoadSuccess}
        >
          {pages.map((page, index) => (
            <div key={index}>
              <Page
                canvasBackground="#FFF"
                width={width}
                pageNumber={page}
                className={styles.pdfview__page}
              />
              {page !== pages.length && <br />}
            </div>
          ))}
        </Document>
      </div>
      <div className={styles.footer}>
        <button
          onClick={() => setModalOpen(false)}
          className={[styles.button, styles.button__red].join(' ')}
        >
          I DONT`T AGREE
        </button>
        <button
          onClick={() => (window.location = downloadLink)}
          className={[styles.button, styles.button__green].join(' ')}
        >
          SURE, I AGREE!
        </button>
      </div>
    </>
  );
};

export default Rules;
