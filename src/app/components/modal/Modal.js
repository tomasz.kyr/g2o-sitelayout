'use client';

import styles from '@/app/styles/modal.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

import ScrollLock from '@/app/components/ScrollLock';
import { useModalContext } from '@/app/components/modal/ModalProvider';

const Modal = ({ children }) => {
  const { setModalOpen } = useModalContext();
  return (
    <div className={styles.body}>
      <div onClick={() => setModalOpen(false)} className={styles.background} />
      <div className={styles.dialog}>
        <div className={styles.container}>
          <div className={styles.header}>
            <button
              onClick={() => setModalOpen(false)}
              className={styles['x-sign']}
            >
              <FontAwesomeIcon icon={faXmark} />
            </button>
          </div>
          <div className={styles.main}>{children}</div>
        </div>
      </div>
      <ScrollLock />
    </div>
  );
};

export default Modal;
