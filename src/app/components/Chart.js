'use client';

import {
  ResponsiveContainer,
  AreaChart,
  Area,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
} from 'recharts';

const Chart = ({ data }) => (
  <ResponsiveContainer aspect={1.75}>
    <AreaChart data={data}>
      <XAxis dataKey="day" />
      <YAxis dataKey="count" />
      <CartesianGrid strokeDasharray="4 4" />
      <Area type="monotone" dataKey="count" stroke="#ffb320" fill="#ffb320" />
      <Tooltip />
    </AreaChart>
  </ResponsiveContainer>
);

export default Chart;
