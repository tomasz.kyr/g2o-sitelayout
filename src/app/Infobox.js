/* eslint-disable react/display-name */

'use client';

import React, { useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import Image from 'next/image';

import info_box_0 from '@/assets/info_box_0.webp';
import info_box_1 from '@/assets/info_box_1.webp';
import info_box_2 from '@/assets/info_box_2.webp';

import styles from '@/app/styles/infobox.module.css';

const MotionImage = motion(
  React.forwardRef((props, ref) =>
    React.createElement(Image, { ...props, ref: ref })
  )
);

const InfoBox = () => {
  const infoList = [
    [
      info_box_0,
      'Community',
      'Our community consists of many different people. You can find creators here, as well as players. Some, are able to create immersive adventures, interesting maps, atmospheric items and provide hours of gameplay for all those who are just waiting to move into their favorite universe. The others, on the other hand, create wonderful memories by spending time together during hundreds of adventures.',
    ],
    [
      info_box_1,
      'Platform',
      'Our platform allows you to create, as well as later play on jointly created projects. Every willing creator, here has a chance to give vent to his creativity to create something that will provide him, as well as others dozens of hours of playing together. All you need is a bit of willingness, time, and you will certainly be able to create something.',
    ],
    [
      info_box_2,
      'Servers',
      'You can find servers of really every category with us. Through DM, where players devote their time, practicing the perfect timing of animations to better fight, through the immersive worlds of RP servers, which allow you to experience dozens of different stories, ending with MMORPG mode, where, together with your clan, you can dominate the entire server.',
    ],
  ];
  const [activeIndex, setActiveIndex] = useState(0);
  return (
    <section className={styles.section}>
      <div className={styles.container}>
        <div className={styles.infolist}>
          <div className={styles.infolist__box}>
            <AnimatePresence initial={false} mode="wait">
              <MotionImage
                key={activeIndex}
                className={styles.infolist__img}
                alt="info box picture"
                width="550"
                height="450"
                src={infoList[activeIndex][0]}
                initial={{ opacity: 0 }}
                exit={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                priority
              ></MotionImage>
            </AnimatePresence>
          </div>
          <div
            className={[
              styles.infolist__box,
              styles['infolist__box--textlist'],
            ].join(' ')}
          >
            {infoList.map((server, index) => (
              <div
                key={index}
                onClick={() => setActiveIndex(index)}
                className={
                  activeIndex === index
                    ? [
                        styles.infolist__text,
                        styles['infolist__text--active'],
                      ].join(' ')
                    : styles.infolist__text
                }
              >
                <h2 className={styles.h2}>{server[1]}</h2>
                <p className={styles.p}>{server[2]}</p>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default InfoBox;
