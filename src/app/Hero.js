'use client';

import styles from '@/app/styles/hero.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faCirclePlay } from '@fortawesome/free-solid-svg-icons';
import { useModalContext } from './components/modal/ModalProvider';

const Hero = () => {
  const { setModalOpen } = useModalContext();
  return (
    <header className={styles.hero}>
      <div className={styles.container}>
        <div className={styles.hello}>
          <h1 className={styles.h1}>
            Join the community that loves the
            <br />
            Gothic universe!
          </h1>
          <p className={styles.p}>
            Develop your character on an MMO server, participate in clan
            tournaments, or
            <br /> immerse yourself in a magical RolePlay world!
          </p>
        </div>
        <div className={styles.joinus}>
          <button className={styles.button} onClick={() => setModalOpen(true)}>
            Join us!
          </button>
          <a
            className={styles.link}
            target="_blank"
            rel="noopener noreferrer"
            href="https://www.youtube.com/watch?v=xcSrUCGeAWY"
          >
            <span className={styles.link__text}>Watch trailer</span>
            <FontAwesomeIcon
              className={styles.link__icon}
              icon={faCirclePlay}
            />
          </a>
        </div>
      </div>
    </header>
  );
};

export default Hero;
