'use client';

import { useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';

import { useModalContext } from '@/app/components/modal/ModalProvider';

import Logo from '@/assets/black_logo.gif';
import styles from '@/app/styles/navbar.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faDiscord, faFacebookF } from '@fortawesome/free-brands-svg-icons';

const Navbar = () => {
  const [isToggled, setToggle] = useState(false);
  const { setModalOpen } = useModalContext();
  return (
    <div className={styles.navbar}>
      <div className={styles.container}>
        <div className={styles.topbox}>
          <Link alt="logo" href="/">
            <Image
              alt="logo"
              width="150"
              height="60"
              className={styles.logo}
              src={Logo}
            />
          </Link>
          <span
            onClick={() => setToggle(!isToggled)}
            className={
              isToggled
                ? [styles.toggle, styles['toggle--active']].join(' ')
                : styles.toggle
            }
          >
            <span className={styles.toggle__line}></span>
            <span className={styles.toggle__line}></span>
            <span className={styles.toggle__line}></span>
          </span>
        </div>
        <div
          className={
            isToggled
              ? [styles.bottombox, styles['bottombox--active']].join(' ')
              : styles.bottombox
          }
        >
          <nav className={styles.nav}>
            <ol className={styles.list}>
              <li className={styles.nav__item}>
                <Link
                  className={styles.nav__link}
                  onClick={() => setToggle(false)}
                  href="/"
                >
                  Home
                </Link>
              </li>
              <li className={styles.nav__item}>
                <a
                  className={styles.nav__link}
                  href="https://forum.gothic-online.com.pl/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Forum
                </a>
              </li>
              <li className={styles.nav__item}>
                <a
                  className={styles.nav__link}
                  href="https://gothicmultiplayerteam.gitlab.io/docs/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Docs
                </a>
              </li>
              <li
                className={[
                  styles.nav__item,
                  styles['nav__item--sublist'],
                ].join(' ')}
              >
                <span
                  className={[
                    styles.nav__link,
                    styles['nav__link--sublist'],
                  ].join(' ')}
                >
                  GitLab
                </span>
                <ol className={styles.sublist}>
                  <li className={styles.nav__subitem}>
                    <a
                      className={styles.nav__link}
                      href="https://gitlab.com/GothicMultiplayerTeam"
                      target="_blank"
                      rel="noreferrer"
                    >
                      Offical project
                    </a>
                  </li>
                  <li className={styles.nav__subitem}>
                    <a
                      className={styles.nav__link}
                      href="https://gitlab.com/g2o"
                      target="_blank"
                      rel="noreferrer"
                    >
                      Community projects
                    </a>
                  </li>
                </ol>
              </li>
              <li className={styles.nav__item}>
                <Link
                  className={styles.nav__link}
                  onClick={() => setToggle(false)}
                  href="/server-list"
                >
                  Servers
                </Link>
              </li>
              <li className={styles.nav__item}>
                <a
                  className={styles.nav__link}
                  href="https://gitlab.com/GothicMultiplayerTeam/GothicMultiplayer/-/issues"
                  target="_blank"
                  rel="noreferrer"
                >
                  BugTracker
                </a>
              </li>
            </ol>
          </nav>
          <div className={styles.buttonbox}>
            <button
              className={styles.button}
              onClick={() => setModalOpen(true)}
            >
              Download
            </button>
            <a
              aria-label="Discord link"
              className={styles.nav__stack}
              href="https://discord.gg/gothic-online-platform-683741400374050841"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon icon={faDiscord} />
            </a>
            <a
              aria-label="Facebook link"
              className={styles.nav__stack}
              href="https://www.facebook.com/gothiconline/?locale=pl_PL"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon icon={faFacebookF} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
