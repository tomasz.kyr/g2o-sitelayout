import styles from '@/app/styles/questionbox.module.css';

const QuestionBox = () => (
  <section className={styles.section}>
    <div className={styles.container}>
      <div className={styles.section__title}>
        <h2 className={styles.h2}>Frequently asked questions</h2>
      </div>
      <div className={styles.asklist}>
        <div className={styles.asklist__box}>
          <div className={styles.asklist__answer}>
            <h3 className={styles.h3}>What is G2O?</h3>
            <p className={styles.p}>
              Gothic 2 Online (G2O) is a free multiplayer modification for
              popular RPG &apos;Gothic II:Night of the Raven 2.6&apos;
              Originally founded in early 2016 as a replacement for almost dead
              GMP/GMP:A. Since then growing into advanced multiplayer platform.
            </p>
            <p className={styles.p}>
              Multiplayer was build using reverse engineering techniques with
              own SDK, later changed to AST SDK. Gothic 2 Online exposes
              original games engine functionality via a scripting language
              &apos;Squirrel&apos;. Using scripts a minimal sandbox style game,
              can be changed to any game mode. Scripting API can be extended by
              external modules. Both server and client supports scripting and
              modules API. The project is developed and maintained by G2O Team
              and remains closed-source for security reasons.
            </p>
          </div>
        </div>
        <div className={styles.asklist__box}>
          <div className={styles.asklist__answer}>
            <h3 className={styles.h3}>What do I need to be able to play?</h3>
            <p className={styles.p}>
              A copy of Gothic 2 with Night of the Raven (the Gold Edition
              already includes this DLC). Also make sure that the version of
              your game is 2.6. You can check this by starting single player and
              looking at the start menu. If you do not have this version, then{' '}
              <a
                className={styles.link}
                href="https://www.worldofgothic.com/dl/?go=dlfile&fileid=122"
                target="_blank"
                rel="noopener noreferrer"
              >
                download and install it
              </a>
              .
            </p>
          </div>
        </div>
        <div className={styles.asklist__box}>
          <div className={styles.asklist__answer}>
            <h3 className={styles.h3}>
              I heard about CO-OP mode, how can I play it with friends?
            </h3>
            <p className={styles.p}>
              You can download it on this{' '}
              <a
                className={styles.link}
                href="https://discord.gg/aM5wChmhJy"
                target="_blank"
                rel="noopener noreferrer"
              >
                Discord
              </a>
              , but keep in mind that it is a separate modification and G2O Team
              does not offer help with problems and bugs resulting from CO-OP.
              Any questions you may have should be asked on the discord
              dedicated to CO-OP or on the official website{' '}
              <a
                className={styles.link}
                href="https://gothiccoop.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                gothiccoop.com
              </a>
              !
            </p>
          </div>
        </div>
        <div className={styles.asklist__box}>
          <div className={styles.asklist__answer}>
            <h3 className={styles.h3}>
              Will graphics mods like DX11 or union plugins work with G2O?
            </h3>
            <p className={styles.p}>
              In case of the DX11 the answer is yes! We recommend{' '}
              <a
                className={styles.link}
                href="https://github.com/SaiyansKing/GD3D11/releases"
                target="_blank"
                rel="noopener noreferrer"
              >
                download
              </a>{' '}
              the latest version in order to avoid problems. Other plugins
              similar to those included in the &apos;UNION&apos; packages should
              generally work, but they may be blocked by individual servers due
              to the unfair advantage they can give to the person using them.
            </p>
          </div>
        </div>
        <div className={styles.asklist__box}>
          <div className={styles.asklist__answer}>
            <h3 className={styles.h3}>
              When trying to connect to the server, Error Code 2 pops up
            </h3>
            <p className={styles.p}>
              The problem occurs when you try to connect to the server using a
              virtual machine (an example is Sandboxie) For security reasons,
              G2O does not allow this. Try connecting to the server normally,
              without a virtual machine.
            </p>
            <p className={styles.p}>
              The other reason might be an antivirus, like BitDefender, try
              adding Gothic2.exe to the antivirus exceptions list.
            </p>
          </div>
        </div>
        <div className={styles.asklist__box}>
          <div className={styles.asklist__answer}>
            <h3 className={styles.h3}>
              I found a bug during the game. How can I report it?
            </h3>
            <p className={styles.p}>
              Describe it on our &apos;BugTracker&apos; or let us know on the
              project&apos;s Discord. Ideally, you should describe your problem
              using pictures or videos.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default QuestionBox;
