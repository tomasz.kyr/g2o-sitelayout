import styles from '@/app/styles/notfound.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faScrewdriverWrench } from '@fortawesome/free-solid-svg-icons';

const NotFound = () => {
  return (
    <section className={styles.section}>
      <header className={styles.header}>
        <h2 className={styles.title}>PAGE NOT FOUND</h2>
      </header>
      <div className={styles.text}>
        <span className={styles.icon}>
          <FontAwesomeIcon icon={faScrewdriverWrench} />
        </span>
        <h3 className={styles.h3}>
          <a className={styles.link} href="/">
            This path is not available! Go back to main page...
          </a>
        </h3>
      </div>
    </section>
  );
};

export default NotFound;
