import styles from '@/app/styles/footer.module.css';

const Footer = () => (
  <footer className={styles.footer}>
    <div className={styles.container}>
      <div className={styles.group}>
        <a
          className={styles.link}
          href="https://forum.gothic-online.com.pl/topic/7-forum-regulations/"
          target="_blank"
          rel="noreferrer"
        >
          Site rules
        </a>
        <a
          className={styles.link}
          href="https://forum.gothic-online.com.pl/topic/5-server-regulations/"
          target="_blank"
          rel="noreferrer"
        >
          Server rules
        </a>
      </div>
      <hr className={styles.hr} />
      <div className={styles.group}>
        <h3 className={styles.h3}>
          The Gothic brand is owned by THQ Nordic. The Gothic Online team only
          uses it for non-commercial purposes.
        </h3>
        <div className={styles.copyright}>
          <p className={styles.p}>
            Copyright {new Date().getFullYear()} Gothic-Online.com.pl
          </p>
          <p className={styles.p}>All rights reserved.</p>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
