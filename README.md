## Deploying

If you want to deploy this project with docker you need to set up your proxy with proper ports...

```bash
docker run -d -p 127.0.0.1:3000:3000 --pull=always registry.gitlab.com/tomasz-k/g2o-sitelayout:latest
```

## Author note

This project was created per request for Gothic Online Team, live version can be visited at (https://gothic-online.com.pl/).

## Main page

![Main page](https://gitlab.com/tomasz-k/g2o-sitelayout/-/raw/main/.readme/main.png)

## Server list

![Server list](https://gitlab.com/tomasz-k/g2o-sitelayout/-/raw/main/.readme/list.png)

## Download modal

![Download modal](https://gitlab.com/tomasz-k/g2o-sitelayout/-/raw/main/.readme/modal.png)

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.js`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
