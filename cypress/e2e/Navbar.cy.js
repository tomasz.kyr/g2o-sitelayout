describe('Navigation test', () => {
  it('Open main page', () => {
    cy.visit('/');
    cy.get('li').contains('Home').should('have.attr', 'href', '/').click();
    cy.url().should('include', '/');
  });

  it('Open forum', () => {
    cy.visit('/');
    cy.get('li')
      .contains('Forum')
      .should('have.attr', 'href', 'https://forum.gothic-online.com.pl/')
      .click();
  });

  it('Open docs', () => {
    cy.visit('/');
    cy.get('li')
      .contains('Docs')
      .should(
        'have.attr',
        'href',
        'https://gothicmultiplayerteam.gitlab.io/docs/'
      )
      .click();
  });

  it('Open GitLab resources', () => {
    cy.visit('/');
    cy.get('li')
      .contains('GitLab')
      .get('a[href="https://gitlab.com/GothicMultiplayerTeam"]')
      .should('have.attr', 'href', 'https://gitlab.com/GothicMultiplayerTeam')
      .click({ force: true });
  });

  it('Open server list', () => {
    cy.visit('/');
    cy.get('li')
      .contains('Servers')
      .should('have.attr', 'href', '/server-list')
      .click();
    cy.url().should('include', '/server-list');
  });

  it('Open issues', () => {
    cy.visit('/');
    cy.get('li')
      .contains('BugTracker')
      .should(
        'have.attr',
        'href',
        'https://gitlab.com/GothicMultiplayerTeam/GothicMultiplayer/-/issues'
      )
      .click();
  });

  it('Open download modal', () => {
    cy.visit('/');
    cy.get('button').contains('Download').click({ force: true });
    cy.get('div[class*="modal_body"]').should('exist');
  });

  it('Open discord', () => {
    cy.visit('/');
    cy.get('a[aria-label*="Discord"]').should('exist').click();
  });

  it('Open facebook', () => {
    cy.visit('/');
    cy.get('a[aria-label*="Facebook"]').should('exist').click();
  });
});
