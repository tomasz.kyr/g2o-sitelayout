describe('Footer', () => {
  it('Open forum rules', () => {
    cy.visit('/');
    cy.get(
      'a[href*="https://forum.gothic-online.com.pl/topic/7-forum-regulations/"]'
    )
      .should('exist')
      .click();
  });

  it('Open servers rules', () => {
    cy.visit('/');
    cy.get(
      'a[href*="https://forum.gothic-online.com.pl/topic/5-server-regulations/"]'
    )
      .should('exist')
      .click();
  });
});
