describe('Hero header test', () => {
  it('Open download modal', () => {
    cy.visit('/');
    cy.get('button').contains('Join us!').click();
    cy.get('div[class*="modal_body"]').should('exist');
  });

  it('Open discord', () => {
    cy.visit('/');
    cy.get('a[href*="https://www.youtube.com/watch?v=xcSrUCGeAWY"]')
      .should('exist')
      .click();
  });
});
