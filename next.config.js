/** @type {import('next').NextConfig} */

const nextConfig = {
  webpack: (config) => {
    config.module.rules.push({
      test: /\.node/,
      use: 'raw-loader',
    }),
      (config.externals = [...config.externals, 'canvas']);
    return config;
  },
  images: {
    domains: ['api.gothic-online.com.pl', 'gothic-online.com.pl'],
  },
  output: 'standalone',
  reactStrictMode: false,
};

module.exports = nextConfig;
